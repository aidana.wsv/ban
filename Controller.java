import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import java.util.Optional;

public class Controller implements ActionListener {
    private final Model model;

    public Controller(Viewer viewer) {
        model = new Model(viewer);
    }

    @Override
    public void actionPerformed(ActionEvent event) {
        JButton button = (JButton) event.getSource();
        String label = button.getText();
        Optional<CalcButton> calcButton = CalcButton.getByLabel(label);
        model.doAction(calcButton.get());
    }
}
