import javax.swing.JFrame;
import javax.swing.JTextField;
import javax.swing.JPanel;
import javax.swing.JButton;
import java.awt.Color;
import java.awt.Font;
import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Optional;

public class Viewer {
    private JFrame frame;
    private JTextField expressionField;
    private JTextField resultField;
    private Controller controller;

    public Viewer() {
        controller = new Controller(this);
        frame = new JFrame("Calculator");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setSize(400, 400);
        frame.getContentPane().setBackground(Color.BLACK);

        expressionField = new JTextField();
        expressionField.setEditable(false);
        expressionField.setFont(new Font("Arial", Font.PLAIN, 24));
        expressionField.setBackground(Color.BLACK);
        expressionField.setForeground(Color.WHITE);

        resultField = new JTextField("0");
        resultField.setEditable(false);
        resultField.setFont(new Font("Arial", Font.PLAIN, 32));
        resultField.setHorizontalAlignment(JTextField.RIGHT);
        resultField.setBackground(Color.BLACK);
        resultField.setForeground(Color.RED);

        JPanel buttonPanel = createButtonPanel();

        frame.setLayout(new BorderLayout());
        frame.add(expressionField, BorderLayout.NORTH);
        frame.add(resultField, BorderLayout.CENTER);
        frame.add(buttonPanel, BorderLayout.SOUTH);

        frame.setVisible(true);
    }

    private JButton createStyledButton(String label, Color background, Color foreground) {
        JButton button = new JButton(label);
        button.setFont(new Font("Arial", Font.PLAIN, 24));
        button.setBackground(background);
        button.setForeground(foreground);
        button.addActionListener(controller);

        return button;
    }

    private JPanel createButtonPanel() {
        JPanel panel = new JPanel();
        panel.setLayout(new GridLayout(5, 4));

        JButton button7 = createStyledButton("7", Color.GRAY, Color.WHITE);
        panel.add(button7);

        JButton button8 = createStyledButton("8", Color.GRAY, Color.WHITE);
        panel.add(button8);

        JButton button9 = createStyledButton("9", Color.GRAY, Color.WHITE);
        panel.add(button9);

        JButton buttonDivide = createStyledButton("/", Color.GRAY, Color.WHITE);
        panel.add(buttonDivide);

        JButton button4 = createStyledButton("4", Color.GRAY, Color.WHITE);
        panel.add(button4);

        JButton button5 = createStyledButton("5", Color.GRAY, Color.WHITE);
        panel.add(button5);

        JButton button6 = createStyledButton("6", Color.GRAY, Color.WHITE);
        panel.add(button6);

        JButton buttonMultiply = createStyledButton("*", Color.GRAY, Color.WHITE);
        panel.add(buttonMultiply);

        JButton button1 = createStyledButton("1", Color.GRAY, Color.WHITE);
        panel.add(button1);

        JButton button2 = createStyledButton("2", Color.GRAY, Color.WHITE);
        panel.add(button2);

        JButton button3 = createStyledButton("3", Color.GRAY, Color.WHITE);
        panel.add(button3);

        JButton buttonSubtract = createStyledButton("-", Color.GRAY, Color.WHITE);
        panel.add(buttonSubtract);

        JButton button0 = createStyledButton("0", Color.GRAY, Color.WHITE);
        panel.add(button0);

        JButton buttonDecimal = createStyledButton(".", Color.GRAY, Color.WHITE);
        panel.add(buttonDecimal);

        JButton buttonEquals = createStyledButton("=", Color.GRAY, Color.WHITE);
        panel.add(buttonEquals);

        JButton buttonAdd = createStyledButton("+", Color.GRAY, Color.WHITE);
        panel.add(buttonAdd);

        JButton buttonC = createStyledButton("C", Color.GRAY, Color.BLACK);
        panel.add(buttonC);

        JButton buttonAC = createStyledButton("AC", Color.GRAY, Color.BLACK);
        panel.add(buttonAC);

        JButton buttonLeftParen = createStyledButton("(", Color.GRAY, Color.WHITE);
        panel.add(buttonLeftParen);

        JButton buttonRightParen = createStyledButton(")", Color.GRAY, Color.WHITE);
        panel.add(buttonRightParen);

        return panel;
    }

    public void updateExpression(String expression) {
        expressionField.setText(expression);
    }

    public void updateResult(double result) {
        resultField.setText(Double.toString(result));
    }

    public static void main(String[] args) {
        new Viewer();
    }
}
