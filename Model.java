import java.util.List;
import java.util.ArrayList;
import java.util.stream.Collectors;

/**
 * Represents the model of a calculator.
 * This model stores a list of pressed buttons and
 * uses the Reverse Polish Notation algorithm to calculate the result.
 * As well as the infix notation form
 */
public class Model {

    private Viewer viewer;
    private ReversePolishNotation algorithm;
    private List<CalcButton> buttons;

    public Model(Viewer viewer) {
        this.viewer = viewer;
        algorithm = new ReversePolishNotation(this);
        buttons = new ArrayList<>();
    }

    /**
     * Accepts a new button input from the user, updates the internal representation
     * of the infix expression, and recalculates the result using the specified algorithm.
     *
     * @param button The calculator button pressed by the user.
     */
    public void doAction(CalcButton button) {
        switch (button) {
            case ZERO:
            case ONE:
            case TWO:
            case THREE:
            case FOUR:
            case FIVE:
            case SIX:
            case SEVEN:
            case EIGHT:
            case NINE:
            case POINT:
            case OPEN_PARENTHESIS:
            case CLOSE_PARENTHESIS:
            case ADD:
            case MULTIPLY:
            case DIVIDE:
            case SUBTRACT:
                buttons.add(button);
                break;
            case CLEAR:
                if (buttons.size() > 0) {
                    buttons.remove(buttons.size() - 1);
                }
                viewer.updateResult(0);
                break;
            case CLEAR_ALL:
                buttons.clear();
                viewer.updateResult(0);
                viewer.updateExpression("0");
                break;
            case EQUAL:
                algorithm.calc(generateInfix());
                break;
            default:
                throw new RuntimeException("CalcButton not found: " + button);
        }
        viewer.updateExpression(generateInfix());
    }

    public void updateResult(double result) {
        viewer.updateResult(result);
    }

    /**
     * Generates an infix expression based on an array of buttons pressed by the user.
     * If the array is empty, it returns "0".
     *
     * @return infix expression or "0" if the button array is empty
     */
    private String generateInfix() {
        String infix = buttons.stream()
            .map(CalcButton::getLabel)
            .collect(Collectors.joining(""));

        return (infix.isEmpty()) ? "0" : infix;
    }
}
