import java.util.Optional;

public class Stack<T> {

    private static class Node<T> {
        public T item;
        public Node<T> deep;

        public Node(T item, Node<T> deep) {
            this.item = item;
            this.deep = deep;
        }
    }

    private Node<T> top;

    /**
     * Removes the object at the top of this stack and returns
     * that object as the value of this function.
     *
     * @return The object at the top of this stack.
     */
    public Optional<T> pop() {
        T item = top.item;
        top = top.deep;
        return Optional.ofNullable(item);
    }

    /**
     * Pushes an item onto the top of this stack.
     *
     * @param item the item to be pushed onto this stack.
     */
    public void push(T item) {
        Node<T> newNode = new Node<>(item, top);
        top = newNode;
    }

    public Optional<T> peek() {
        Optional<T> peek = Optional.ofNullable(top.item);
        return peek;
    }

    /**
     * Tests if this stack is empty.
     *
     * return true if and only if this stack contains no items; false otherwise.
     */
    public boolean isEmpty() {
        return top == null;
    }

}
