import java.lang.ArithmeticException;
import java.lang.IllegalArgumentException;

/**
 * The class implements the Reverse Polish Notation (RPN) algorithm.
 */
public class ReversePolishNotation {

    private Model model;

    public ReversePolishNotation (Model model){
        this.model = model;
    }

    /**
     * Calculates the result based on the provided infix expression.
     * & Update model
     *
     * @param infix The infix expression to be calculated.
     */
    public void calc(String infix) {
        String postfix = getPostfix(infix);
        System.out.println("postfix: [" + postfix + "]");
        double result = calcResult(postfix);
        model.updateResult(result);
    }

    /**
     * Generates a postfix form from an infix form.
     *
     * @param infix String infix expression
     * @return postfix expression
     */
    public String getPostfix(String infix) {
        Stack<CalcButton> operators = new Stack<>();
        StringBuilder postfix = new StringBuilder();

        for (char ch : infix.toCharArray()) {
            if (isDigit(ch)) {
                postfix.append(ch);
            } else if (ch  == CalcButton.OPEN_PARENTHESIS.getLabel().charAt(0)) {
                operators.push(CalcButton.OPEN_PARENTHESIS);
            } else if (ch == CalcButton.CLOSE_PARENTHESIS.getLabel().charAt(0)) {
                while (!operators.isEmpty() && operators.peek().get() != CalcButton.OPEN_PARENTHESIS) {
                    postfix.append(" ")
                        .append(operators.pop().get().getLabel());
                }
                // remove OPEN_PARENTHESIS
                if (!operators.isEmpty() && operators.peek().get() == CalcButton.OPEN_PARENTHESIS) {
                    operators.pop();
                }
            } else {
                postfix.append(" ");
                // only operators without PARENTHESIS
                while (!operators.isEmpty() && getPrecedence(CalcButton.getByLabel(String.valueOf(ch)).get()) <= getPrecedence(operators.peek().get())) {
                    postfix.append(" ")
                        .append(operators.pop().get().getLabel())
                        .append(" ");
                }
                operators.push(CalcButton.getByLabel(String.valueOf(ch)).get());
            }
        }

        while (!operators.isEmpty()) {
            postfix.append(" ")
                .append(operators.pop().get().getLabel());
        }

        // replace all double space to one space
        return postfix.toString().replaceAll(" +", " ").trim();
    }

    /**
     * Calculate result based on postfix string
     *
     * @param postfix String postfix expression
     * @return double result of calculation
     */
    public double calcResult(String postfix) {
        Stack<Double> operands = new Stack<>();
        String[] lems = postfix.split(" ");

        for (String lem : lems) {
            if (isDigit(lem.charAt(0))) {
                operands.push(Double.parseDouble(lem));
            } else {
                // only operator
                double operand1;
                double operand2;
                if (!operands.isEmpty()) {
                    operand2 = operands.pop().get();
                    if (!operands.isEmpty()) {
                        operand1 = operands.pop().get();
                    } else {
                        return 0;
                    }
                } else {
                    return 0;
                }

                double result = performOperation(CalcButton.getByLabel(lem).get(), operand1, operand2);
                operands.push(result);
            }
        }

        return operands.pop().get();
    }

    /**
     * Tests a character is a number, including a dot.
     */
    private boolean isDigit(char ch) {
        String digits =
            CalcButton.ZERO.getLabel() +
            CalcButton.ONE.getLabel() +
            CalcButton.TWO.getLabel() +
            CalcButton.THREE.getLabel() +
            CalcButton.FOUR.getLabel() +
            CalcButton.FIVE.getLabel() +
            CalcButton.SIX.getLabel() +
            CalcButton.SEVEN.getLabel() +
            CalcButton.EIGHT.getLabel() +
            CalcButton.NINE.getLabel() +
            CalcButton.POINT.getLabel();

        return digits.indexOf(ch) != -1;
    }

    /**
     * Returns the precedence of operator
     * the higher the number, the higher the priority
     *
     * @param operator CalcButton Operator as a button pressed by the user
     * @return int precedence of operator
     */
    private int getPrecedence(CalcButton operator) {
        switch (operator) {
            case ADD:
            case SUBTRACT:
                return 1;
            case MULTIPLY:
            case DIVIDE:
                return 2;
            default:
                return 0;
        }
    }

    /**
     * Performing mathematical operations
     *
     * @param operator CalcButton mathematical operator
     * @param operand1 double first operand
     * @param operand2 double second operand
     */
    private double performOperation(CalcButton operator, double operand1, double operand2) {
        switch (operator) {
            case ADD:
                return operand1 + operand2;
            case SUBTRACT:
                return operand1 - operand2;
            case MULTIPLY:
                return operand1 * operand2;
            case DIVIDE:
                if (operand2 == 0) {
                    throw new ArithmeticException("Division by zero");
                }
                return operand1 / operand2;
            default:
                throw new IllegalArgumentException("Invalid operator: " + operator);
        }
    }

}
